#!/bin/sh

set -xeuo pipefail

IMAGE="visionfive-$(date +%Y%m%d).img"
MNTPNT="$(mktemp -d /tmp/starfive.XXXXXX)"
LOOP_DEVICE="$(losetup -f)"

if [ "$(id -u)" -ne 0 ]; then
	echo "Script needs to be run as root" 1>&2
	exit 1
fi

##
# Create image file.
##

cleanup() {
	set +e
	sync
	umount "$MNTPNT/boot" "$MNTPNT"
	losetup -d "${LOOP_DEVICE}"
	rm -rf "$MNTPNT"
}

trap cleanup INT EXIT

rm -f "$IMAGE"
truncate -s 500M "$IMAGE"

# XXX: The off-the-shelf firmware with "U-Boot 2022.04-rc2-VisionFive"
# hardcodes the U-Boot bootpart variable to "bootpart=0:3". Thus, it
# reads the uEnv.txt from partition 3. Since we can't change U-Boot
# configurations before uEnv.txt is loaded our first partition is
# partition 3 for now.
sfdisk "${IMAGE}" <<EOF
3: start=34,size=250M,type=L,bootable
4: start=,size=200M,type=L
EOF

losetup -P "${LOOP_DEVICE}" "${IMAGE}"

for partition in 3 4; do
	if [ ! -b "${LOOP_DEVICE}p${partition}" ]; then
		echo "Partition '${LOOP_DEVICE}p${partition}' does not exist" 1>&2
		exit 1
	fi
done

##
# Mount
##

mkfs.ext4 "${LOOP_DEVICE}p4"
mount -text4 "${LOOP_DEVICE}p4" "$MNTPNT"

mkdir -p "$MNTPNT/boot"
mkfs.ext4 "${LOOP_DEVICE}p3"
mount -text4 "${LOOP_DEVICE}p3" "$MNTPNT/boot"

##
# Bootloader Configuration
##

mkdir -p "$MNTPNT"/boot/extlinux
cat <<EOF > "$MNTPNT"/boot/extlinux/extlinux.conf
menu title StarFive VisionFive
timeout 50
default alpine

label alpine
	menu label Alpine Linux
	kernel /vmlinuz-starfive
	fdt /dtbs-starfive/starfive/jh7100-starfive-visionfive-v1.dtb
	append earlycon=sbi rw root=/dev/mmcblk0p4 rootfstype=ext4 rootwait console=ttyS0,115200
EOF

mkdir -p "$MNTPNT"/boot/boot
cat <<EOF > "$MNTPNT"/boot/boot/uEnv.txt
fdt_high=0xffffffffffffffff
initrd_high=0xffffffffffffffff

scriptaddr=0x88100000
script_offset_f=0x1fff000
script_size_f=0x1000

kernel_addr_r=0x84000000
kernel_comp_addr_r=0x90000000
kernel_comp_size=0x10000000

fdt_addr_r=0x88000000
ramdisk_addr_r=0x88300000

efi_dtb_prefixes=/ /dtbs-edge/ /dtbs-starfive/

bootcmd=devnum=0; run mmc_boot
bootcmd_mmc0=devnum=0; run mmc_boot
distro_bootcmd=run bootcmd_mmc0
EOF

##
# Alpine rootfs.
##

# TODO: Alpine doesn't generic release images for RISC-V yet.
# For this reason, we are forced to create our own rootfs for now.
#
# This requires installing qemu-riscv64 and qemu-openrc as well as
# starting the qemu-binfmt OpenRC services. Otherwise, the post-install
# scripts cannot be executed.
apk --quiet --root "$MNTPNT" --arch riscv64 --initdb --update-cache --allow-untrusted \
	-X http://dl-cdn.alpinelinux.org/alpine/edge/main \
	-X http://dl-cdn.alpinelinux.org/alpine/edge/community \
	-X "$(pwd)/pkg/starfive" \
	add alpine-base linux-starfive linux-firmware-none ca-certificates

# Activate various OpenRC services by default
ln -s /etc/init.d/bootmisc "$MNTPNT"/etc/runlevels/boot/
ln -s /etc/init.d/hostname "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/modules "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/sysctl "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/urandom "$MNTPNT"/etc/runlevels/boot
ln -s /etc/init.d/devfs "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/hwdrivers "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/mdev "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/modules "$MNTPNT"/etc/runlevels/sysinit
ln -s /etc/init.d/mount-ro "$MNTPNT"/etc/runlevels/shutdown
ln -s /etc/init.d/killprocs "$MNTPNT"/etc/runlevels/shutdown

# Hack to enable autologin on ttyS0
printf '\n# Auto login on SiFive serial\n%s\n' \
	'ttyS0::respawn:/bin/login -f root' \
	>> "$MNTPNT"/etc/inittab

# See https://github.com/starfive-tech/linux/issues/54
# For some reason loading this module results in a kernel panic.
echo "blacklist snd_soc_starfive_i2svad" \
	> "$MNTPNT"/etc/modprobe.d/blacklist-i2svad.conf
