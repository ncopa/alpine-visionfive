#!/bin/sh
set -e

export CBUILD=riscv64

exec buildrepo \
	--aports "$(pwd)" \
	--destdir "$(pwd)/pkg" \
	--purge \
	--rootbld \
	starfive
